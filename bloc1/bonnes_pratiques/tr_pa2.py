def factorielle(n):
    fact = 1
    for i in range(2, n+1):
        fact *= i
    return fact

def binomial(n, p):
    return factorielle(n) // (factorielle(n-p)*factorielle(p))

def cree_triangle_pascal(nmax):
    triangle = []
    for n in range(nmax+1):
        ligne = []
        for p in range(n+1):
            ligne.append(binomial(n, p))
        triangle.append(ligne)
    return triangle
    
def imprimer_ligne(liste):
    for k in liste:
        print(k, end=' ')
    print()
        
def imprimer_triangle(liste_listes):
    for liste in liste_listes:
        imprimer_ligne(liste)



N_MAX = 6
imprimer_triangle(cree_triangle_pascal(N_MAX))

